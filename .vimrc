" .vimrc - My Vim config ;)
"
" Written in 2017 by Caleb Herbert <csh@bluehome.net>
"
" To the extent possible under law, the author(s) have dedicated all
" copyright and related and neighboring rights to this software to the
" public domain worldwide. This software is distributed without any
" warranty.
"
" You should have received a copy of the CC0 Public Domain Dedication
" along with this software. If not, see
" <http://creativecommons.org/publicdomain/zero/1.0/>.

nnoremap i :q!<CR>
nnoremap a :q!<CR>

nnoremap a :q!<CR>
nnoremap b :q!<CR>
nnoremap c :q!<CR>
nnoremap d :q!<CR>
nnoremap e :q!<CR>
nnoremap f :q!<CR>
nnoremap g :q!<CR>
nnoremap h :q!<CR>
nnoremap i :q!<CR>
nnoremap j :q!<CR>
nnoremap k :q!<CR>
nnoremap l :q!<CR>
nnoremap m :q!<CR>
nnoremap n :q!<CR>
nnoremap o :q!<CR>
nnoremap p :q!<CR>
nnoremap q :q!<CR>
nnoremap r :q!<CR>
nnoremap s :q!<CR>
nnoremap t :q!<CR>
nnoremap u :q!<CR>
nnoremap v :q!<CR>
nnoremap w :q!<CR>
nnoremap x :q!<CR>
nnoremap y :q!<CR>
nnoremap z :q!<CR>

nnoremap A :q!<CR>
nnoremap B :q!<CR>
nnoremap C :q!<CR>
nnoremap D :q!<CR>
nnoremap E :q!<CR>
nnoremap F :q!<CR>
nnoremap G :q!<CR>
nnoremap H :q!<CR>
nnoremap I :q!<CR>
nnoremap J :q!<CR>
nnoremap K :q!<CR>
nnoremap L :q!<CR>
nnoremap M :q!<CR>
nnoremap N :q!<CR>
nnoremap O :q!<CR>
nnoremap P :q!<CR>
nnoremap Q :q!<CR>
nnoremap R :q!<CR>
nnoremap S :q!<CR>
nnoremap T :q!<CR>
nnoremap U :q!<CR>
nnoremap V :q!<CR>
nnoremap W :q!<CR>
nnoremap X :q!<CR>
nnoremap Y :q!<CR>
nnoremap Z :q!<CR>

nnoremap 1 :q!<CR>
nnoremap 2 :q!<CR>
nnoremap 3 :q!<CR>
nnoremap 4 :q!<CR>
nnoremap 5 :q!<CR>
nnoremap 6 :q!<CR>
nnoremap 7 :q!<CR>
nnoremap 8 :q!<CR>
nnoremap 9 :q!<CR>
nnoremap 0 :q!<CR>

" unoccupied <3 ;)
nnoremap : :q!<CR>